#!/bin/bash

## 02_03_install_tomcat on ubuntu
bash pwd
echo "RUNNING 02_03_install_tomcat.sh"
## remove old if exists:
echo "sudo rm -r /opt/tomcat/"
sudo rm -r /opt/tomcat/
echo "sudo mkdir /opt/tomcat"
sudo mkdir /opt/tomcat
## https://medium.com/@madeeshafernando/installing-apache-tomcat-on-ubuntu-18-04-8cf8bc63993d
echo "cd /tmp"
cd /tmp
echo "sudo wget https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.33/bin/apache-tomcat-9.0.33.tar.gz"
sudo wget https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.33/bin/apache-tomcat-9.0.33.tar.gz
## extract downloaded the Tomcat archive and move it to the /opt/tomcat directory.
echo "sudo tar xf /tmp/apache-tomcat-9*.tar.gz -C /opt/tomcat"
sudo tar xf /tmp/apache-tomcat-9*.tar.gz -C /opt/tomcat
echo "sudo rm /tmp/apache-tomcat-9*.tar.gz"
sudo rm /tmp/apache-tomcat-9*.tar.gz
echo "sudo mkdir /opt/tomcat/apache-tomcat-KEEPOUT"
sudo mkdir /opt/tomcat/apache-tomcat-KEEPOUT
echo "sudo cp -i -r /opt/tomcat/apache-tomcat-9.0.33/* /opt/tomcat/apache-tomcat-KEEPOUT"
sudo cp -i -r /opt/tomcat/apache-tomcat-9.0.33/* /opt/tomcat/apache-tomcat-KEEPOUT
echo "cd /opt/tomcat/apache-tomcat-9.0.33"
cd /opt/tomcat/apache-tomcat-9.0.33
