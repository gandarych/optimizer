package pl.com.microservices.model;

import java.util.*;
//import java.util.streams.*;

class Solution {

    public int calculate(int[] A) {
    	List<Integer> listB = new ArrayList<Integer>();

	for (int element : A) {
	   if (element % 4 == 0) {
	       listB.add(element);
	   }
	}

	int[] B = listB
	   .stream()
            .mapToInt(Integer::intValue)
            .toArray();

        int bsum = Arrays.stream(B)
            .sum();
            //.getAsInt();

        System.out.println(bsum);
        return bsum;
    }

/*

interface AddableInt{  
    int add(int a, int b);  
}  

interface AddableBigInteger{  
    BigInteger add(BigInteger a, BigInteger b);  
} 

---
        AddableInt addWithLambda = (a, b) -> (a + b);  
        System.out.println(addWithLambda.add(10,20));  

        addWithLambda = (int a, int b) -> (a + b);  
        System.out.println(addWithLambda.add(100,200));  

        AddableBigInteger addBigWithLambda = (a, b) -> (a + b);  
        System.out.println(addBigWithLambda.add(10,20));  

        AddableBigInteger addBigWithLambda = (int a, int b) -> (a + b);  
        System.out.println(addBigWithLambda.add(100,200)); 

*/

}

