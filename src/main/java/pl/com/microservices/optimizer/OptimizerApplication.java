package pl.com.microservices.optimizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class OptimizerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OptimizerApplication.class, args);
	}

	@GetMapping("/hyllo")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hyllo %s! Optimizer app is alive!" , name);
	}

}
